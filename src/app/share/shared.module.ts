/**
 * Created by Chetan on 2017-01-11.
 */

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ErrorMessageService } from "./error-messages.service";
import { ConfigService } from "../config-service";
import { RouterModule } from "@angular/router";


@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule],
    declarations: [

    ],
    exports: [

    ],
    providers: [ErrorMessageService],

})
export class SharedModule {

}
