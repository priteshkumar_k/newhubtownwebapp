/*
    AUTHOR : Sumit Ghotekar
    DESCRIPTION : Use this service to set all error messages and other messages
*/
import { Injectable, OnInit, EventEmitter } from "@angular/core";

/*@Injectable :
    It lets Angular know that a class can be used with the dependency injector.
*/
@Injectable()
export class ErrorMessageService {
	private errorMessages;
	private regex;

	constructor() {
		this.setErrorMessages();
		this.setRegex();
	}

	setErrorMessages() {
		this.errorMessages = {};


		this.errorMessages.projectNameReq = "Project Name  is required";
		this.errorMessages.projectStateNameReq = "Project State is required";
		this.errorMessages.projectTypeReq = "Project Type is required";
		this.errorMessages.proposeDateReq = "Date is required";
		this.errorMessages.litigationRelatedReq = "Litigation related to project is required";
		this.errorMessages.plotNumberReq = "Plot Number is required";
		this.errorMessages.areainsqReq = "Area is required";
		this.errorMessages.aggAreainsqReq = "Aggregate Area is required";
		this.errorMessages.proposeNotSanctionReq = "Propose But Not Sanction Building Count is required";
		this.errorMessages.boundaryReq = "Boundary is required";
		this.errorMessages.totalFSIReq = "Total FSI is required";
		this.errorMessages.selectStateReq = "State is required";
		this.errorMessages.selectDivisionReq = "Division is required";
		this.errorMessages.selectDistrictReq = "District is required";
		this.errorMessages.selectTalukaReq = "Taluka is required";
		this.errorMessages.selectVillageReq = "Village is required";
		this.errorMessages.pinCodeReq = "Pincode is required";
		this.errorMessages.bankNameReq = "Bank Name is  required";
		this.errorMessages.branchNameReq = "Branch Name is required";
		this.errorMessages.IfscCodeReq = "IFSC Code is required";
		this.errorMessages.accNumberReq = "Account Number is required";
		this.errorMessages.bankAddressReq = "Bank Address is required";

		this.errorMessages.mandatoryField = "This is mandatory field"

		//Pattern 
		this.errorMessages.pincodePattern = "Please enter a valid pincode";
		this.errorMessages.emailRegex = "please enter valid Email Id"



	}


	/*	Steps :
		1.import { ErrorMessageService } from "../shared/error-messages.service";
		2.Create its object in your constructor -
			private errorMessageService: ErrorMessageService
		3.Use it in your template -
			eg.{{errorMessageService.getErrorMessages().clientNameReq}}

	*/


	getErrorMessages() {
		return this.errorMessages;
	}

	setRegex() {
		this.regex = {};
		this.regex.emailRegex = "/([a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]{2,50})+\.([a-z]{1}))\w+/";
		this.regex.alphaRegex = ".*";  //"/.*/"; //"^[a-zA-Z][a-zA-Z\\s]+$";
		this.regex.alphaNumRegex = ".*"; // "^[a-zA-Z0-9][a-zA-Z0-9\\s]+$";
		this.regex.mobNumRegex = "[0-9]{10}";
		this.regex.contactNumRegex = "[0-9]{8,12}";
		this.regex.picc = "[0-9]{6}";
		this.regex.numberRegex = "[0-9]{1,5}";
		this.regex.numberOnly = "^[0-9]+$"

	}

	getRegex() {
		return this.regex;
	}







}



