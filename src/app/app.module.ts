import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { AppRoutingModule } from "./app-routing.module";
import { LoginModule } from "./login/login.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { HomeModule } from "./home/home.module";
import { MaterialModule } from '@angular/material';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ConfigService } from "./config-service";
import { ErrorMessageService } from "./share/error-messages.service"
import { MdTooltipModule } from '@angular/material';
import { GoogleMapsModule } from 'google-maps-angular2';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    FormsModule,
    MaterialModule,
    AppRoutingModule,
    LoginModule,
    DashboardModule,
    HomeModule,
    MdTooltipModule,
    GoogleMapsModule.forRoot({
      url: "https://maps.googleapis.com/maps/api/js?key=AIzaSyC3vufeIheOz-1fxTQQYuSn6NDFZ_1mcSQ"
    })

  ],
  providers: [ConfigService, ErrorMessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
