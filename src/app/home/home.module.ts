/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Home Module
	Date Created	: 	04 May 2017
	Date Modified	: 	17 May 2017
*/
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home.component";
import { CommonModule } from "@angular/common";
import { DashboardModule } from '../dashboard/dashboard.module';
import { FormsModule } from '@angular/forms'; // <--- JavaScript import from Angular
import { HomeRoutingModule } from './home.routing';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { UserRegistration } from '../home/user/user-add.component';
import { UserDetailUpdate } from '../home/user/user-update.component';
import { UserDetailView } from '../home/user/user-view.component';
import { DepartmentRegistration } from '../home/department/department.component';
import { DepartmentListComponent } from "../home/department/department-list.component";
import { MdTooltipModule } from '@angular/material';
import { UserListComponent } from "../home/user/user-list.component";
import { AppConfig } from "../home/appConfig/appConfig";
import { LocationTracking } from "../home/locationTracking/locationTracking";
import { MapComponent } from "../home/locationTracking/map/mapComponent";
import { EmployeeRecord } from "../home/employeeRecord/employeeRecord";
import { LocationTrackingServices } from "../home/locationTracking/locationTracking.services";
import { ProjectAddComponent } from "../home/project/project-add.component";
import { ProjectListComponent } from "../home/project/project-list.component";
import { CompanyDetails } from "../home/companyProfile/showCompanyDetail";





@NgModule({

    declarations: [
        AppConfig,
        LocationTracking,
        MapComponent,
        EmployeeRecord,
        HomeComponent,
        UserRegistration,
        DepartmentListComponent,
        DepartmentRegistration,
        UserListComponent,
        ProjectListComponent,
        ProjectAddComponent,
        CompanyDetails,
        UserListComponent,
        UserRegistration,
        UserDetailUpdate,
        UserDetailView


    ],

    imports: [
        CommonModule,
        HomeRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        DashboardModule,
        RouterModule,
        MdTooltipModule
    ],
    exports: [RouterModule, MapComponent],

    providers: []
})
export class HomeModule {


}

