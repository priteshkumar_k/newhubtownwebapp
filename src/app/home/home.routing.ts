/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Home Routing Module
	Date Created	: 	05 June 2017
	Date Modified	: 	05 June 2017
*/
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home.component";
import { DashBoardComponent } from "../dashboard/dashboard.component";
import { ProjectAddComponent } from "../home/project/project-add.component";
import { ProjectListComponent } from "../home/project/project-list.component";
import { UserRegistration } from '../home/user/user-add.component';
import { UserDetailUpdate } from '../home/user/user-update.component';
import { UserDetailView } from '../home/user/user-view.component';

import { DepartmentRegistration } from '../home/department/department.component';
import { DepartmentListComponent } from "../home/department/department-list.component";
import { UserListComponent } from "../home/user/user-list.component";
import { AppConfig } from "../home/appConfig/appConfig";
import { LocationTracking } from "../home/locationTracking/locationTracking";
import { EmployeeRecord } from "../home/employeeRecord/employeeRecord";
import { LocationTrackingServices } from "../home/locationTracking/locationTracking.services";
import { EmployeeRecordServices } from "../home/employeeRecord/employeeRecord.services";
import { CompanyDetails } from "../home/companyProfile/showCompanyDetail";



export const homeRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: DashBoardComponent,
            },
            {
                path: 'add-project',
                component: ProjectAddComponent,
            },
            {
                path: 'company-profile',
                component: CompanyDetails,
            },

            {
                path: 'list-page',
                component: ProjectListComponent,
            },


            {
                path: 'adduser',
                component: UserRegistration,
            },
            {
                path: 'departmentRegistration',
                component: DepartmentRegistration,
            },
            {
                path: 'departmentList',
                component: DepartmentListComponent,
            },
            {
                path: 'userList',
                component: UserListComponent,
            },


            {
                path: 'appConfig',
                component: AppConfig,
            },
            {
                path: 'locationTracking',
                component: LocationTracking,
            },

            {
                path: 'employeeRecord',
                component: EmployeeRecord,
            },
            {
                path: 'addUserDetail',
                component: UserRegistration,
            },
            {
                path: 'updateUserDetail',
                component: UserDetailUpdate,
            },
            {
                path: 'viewUserDetail',
                component: UserDetailView,
            },


        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})
export class HomeRoutingModule {

}