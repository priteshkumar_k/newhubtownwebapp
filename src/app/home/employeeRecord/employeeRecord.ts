/*
    Author : Sumit Ghotekar.
    Description : User registration component.
    Date of creation : 17/05/2017
 */

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { EmployeeRecordServices } from "../../home/employeeRecord/employeeRecord.services";


@Component({
    templateUrl: './employeeRecord.html',
    styleUrls: ['./employeeRecord.css'],
    providers: [EmployeeRecordServices]
})

export class EmployeeRecord {
    private employeeRecord: FormGroup;
    reqestObject: any;
    tracklistArray: any;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,
        private employeeRecordServices: EmployeeRecordServices,
    ) {

        this.employeeRecord = formBuilder.group({
            "date": ["", [Validators.required]],

        });

    }

    getEmployeeRecord() {

        this.reqestObject = this.employeeRecord.value;
        console.log("request", this.reqestObject);
        if (this.reqestObject.date != "") {
            this.employeeRecordServices.employeeRecord(this.reqestObject)
                .subscribe((responseObject) => {
                    console.log(responseObject);
                    console.log("done");
                    this.tracklistArray = responseObject;
                    if (responseObject.statusCode == 1) {
                        alert("Data Not Found")
                        this.tracklistArray = [];
                    }
                    else {
                        for (var i = 0; i < this.tracklistArray.length; i++) {
                            this.tracklistArray[i].check_in_time = new Date(this.tracklistArray[i].check_in_time).getTime();
                            this.tracklistArray[i].check_out_time = new Date(this.tracklistArray[i].check_out_time).getTime();
                            console.log(this.tracklistArray[i].check_in_time);

                        }
                    }
                    // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", this.errorMessageService.getErrorMessages().done);

                },
                err => {
                    console.log("not done");
                    // this.feedbackMessageComponent.updateMessage(true, null, "alert-danger", this.errorMessageService.getErrorMessages().notDone);

                }
                );

        }
        else {
            alert("Please Enter Date")
        }
    }
}