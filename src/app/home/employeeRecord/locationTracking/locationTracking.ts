/*
    Author :priteshkumar kanaujiya.
    Description : Location Tracking.
    Date of creation : 17/05/2017
 */

import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { GoogleMapsModule } from 'google-maps-angular2';
import { GoogleMapsService } from 'google-maps-angular2';
import { Directive, Input } from '@angular/core';
import { MapComponent } from "../../home/locationTracking/map/mapComponent";
import { LocationTrackingServices } from "../../home/locationTracking/locationTracking.services";


declare var google: any;


@Component({
    templateUrl: './locationTracking.html',
    styleUrls: ['./locationTracking.css'],
    providers: [GoogleMapsService, LocationTrackingServices]
})



export class LocationTracking {
    reqestObject: any;
    tracklistArray: any;
    latlongArray: any;
    @ViewChild(MapComponent)
    private gmap: MapComponent;
    private locationTracking: FormGroup;
    constructor(
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,
        private locationTrackingServices: LocationTrackingServices



    ) {

        this.locationTracking = formBuilder.group({
            "user_id": ["", [Validators.required]],
            "to_date": ["", [Validators.required]],
            "from_date": ["", [Validators.required]],


        });

    }

    track(tracklist) {
        this.reqestObject = tracklist;
        console.log("inside track");
        console.log("request", this.reqestObject);
        this.locationTrackingServices.latlongDetail(this.reqestObject)
            .subscribe((responseObject) => {
                console.log(responseObject);
                console.log("done");
                this.latlongArray = responseObject;
                if (responseObject.statusCode == 1) {

                    alert("Data Not Found")
                }
                else {
                    console.log(this.latlongArray)
                    this.gmap.calculateAndDisplayRoute(this.latlongArray);
                }


            },
            err => {
                console.log("not done");

            }
            );

    }

    trackingLocationDetail() {
        console.log("In add button function");

        this.reqestObject = this.locationTracking.value;
        if (this.reqestObject.user_id != "" && this.reqestObject.from_date != "" && this.reqestObject.to_date != "") {


            console.log("request", this.reqestObject);
            this.locationTrackingServices.locationTrackingDetail(this.reqestObject)
                .subscribe((responseObject) => {
                    console.log(responseObject);
                    console.log("done");
                    this.tracklistArray = responseObject;
                    if (responseObject.statusCode == 1) {
                        this.tracklistArray = [];
                        alert("Data Not Found")
                    }
                    else {
                        for (var i = 0; i < this.tracklistArray.length; i++) {
                            this.tracklistArray[i].check_in_time = new Date(this.tracklistArray[i].check_in_time).getTime();
                            this.tracklistArray[i].check_out_time = new Date(this.tracklistArray[i].check_out_time).getTime();
                            console.log(this.tracklistArray[i].check_in_time);
                        }
                        console.log(this.tracklistArray)
                    }

                },
                err => {
                    console.log("not done");
                }
                );
        }
        else {
            alert("All field are Mandatory")
        }
    }




}