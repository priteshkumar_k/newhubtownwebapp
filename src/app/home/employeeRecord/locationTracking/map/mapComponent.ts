
/*
    Author :priteshkumar kanaujiya.
    Description : Location Tracking.
    Date of creation : 17/05/2017
 */

import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { GoogleMapsModule } from 'google-maps-angular2';
import { GoogleMapsService } from 'google-maps-angular2';
import { Directive, Input } from '@angular/core';
declare var google: any;

@Component({
  selector: 'map',
  templateUrl: './map.html',
  styleUrls: ['./map.css']
})

export class MapComponent implements OnInit {
  private locationTracking: FormGroup;
  public startLatLong: any;
  public endLatLong: any;

  ngOnInit() {


    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: { lat: 19.1201834, lng: 72.8709326 }
    });
    directionsDisplay.setMap(map);




  }

  calculateAndDisplayRoute(latlongArray) {
    console.log("in map");


    console.log("abc", latlongArray[0]);
    this.startLatLong = latlongArray[0];
    this.endLatLong = latlongArray[latlongArray.length - 1];
    console.log(this.startLatLong);
    console.log(this.endLatLong);

    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: { lat: 19.1201834, lng: 72.8709326 }
    });
    directionsDisplay.setMap(map);
    var waypts = [];
    var checkboxArray: any[] = latlongArray;
    var j = 1;
    if (checkboxArray.length > 2) {
      window.scrollTo(0, document.body.scrollHeight);
      if (checkboxArray.length > 23 && checkboxArray.length <= 46) {
        j = 2
      }
      if (checkboxArray.length > 46 && checkboxArray.length <= 69) {
        j = 3
      }
      if (checkboxArray.length > 69 && checkboxArray.length <= 92) {
        j = 4
      }
      if ((checkboxArray.length > 93)) {
        console.log("waypoints limit exceed")
      }


      for (var i = 1; i < checkboxArray.length - 1; i = i + j) {
        console.log("in loop")
        waypts.push({
          location: checkboxArray[i].latitude + "," + checkboxArray[i].longitude,
          stopover: true
        });

      }
    }
    else {
      alert("Tracking Record Not Available");
    }

    console.log("waypoints", waypts)

    directionsService.route({
      origin: { lat: this.startLatLong.latitude, lng: this.startLatLong.longitude },
      destination: { lat: this.endLatLong.latitude, lng: this.endLatLong.longitude },
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function (response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }
}