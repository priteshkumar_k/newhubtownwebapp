/*
	Author			:	Sumit Ghotekar
	Description		:   Registration component
	Date Created	: 	17 May 2017
	Date Modified	: 	17 May 2017
*/

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { ConfigService } from "../../config-service";
import { UserServices } from "../user/user.services";


interface ResponseObject {
    statusCode: number;
    message: string;
    result: any;

}


@Component({
    //selector: 'login-component',
    templateUrl: './user-view.component.html',
    styleUrls: ['./user.component.css'],
    providers: [UserServices]
})
export class UserDetailView {
    public responseObject: any;

    private userViewForm: FormGroup;
    public reqestObject: any = {};
    public addressObject: any = {};
    // public responseObject: any = [];
    public recObj: any = {};
    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,
        private userServices: UserServices,
        private configService: ConfigService,


    ) {
        console.log("In constructor");
        this.userViewForm = formBuilder.group({
            "firstName": ["", [Validators.required]],
            "lastName": ["", [Validators.required]],
            "panNumber": ["", [Validators.required]],
            "houseAdd": ["", [Validators.required]],
            "buildingNumber": ["", [Validators.required]],
            "strName": ["", [Validators.required]],
            "locality": ["", [Validators.required]],
            "landmark": ["", [Validators.required]],
            "selectState": ["", [Validators.required]],
            "selectDivision": ["", [Validators.required]],
            "selectDistrict": ["", [Validators.required]],
            "selectTaluka": ["", [Validators.required]],
            "selectVillage": ["", [Validators.required]],
            "pinCode": ["", [Validators.required]],
            "department": ["", [Validators.required]],
            "designation": ["", [Validators.required]],
            "role": ["", [Validators.required]],
            "reportingTo": ["", [Validators.required]],
            "address": ["", [Validators.required]],
            "mobileNumber": ["", [Validators.required]],
            "officeNumber": ["", [Validators.required]],
            "faxNumber": ["", [Validators.required]],
            "emailId": ["", [Validators.required]],


        });

        //  this.buildingRegistrationViewForm.patchValue({
        //     "buildingName": responseObject.buildingCode,

        // }
        // );

    }
    onListPage() {
        this.router.navigate(['buildingList']);
    }

    ngOnInit() {
        this.responseObject = this.configService.getView();
        console.log("abc");
        this.userServices.getUserAddressDetail(this.responseObject)
            .subscribe((responseObject1) => {
                console.log("done");

                console.log(responseObject1);

                this.addressObject = responseObject1;
                console.log("address", this.addressObject)
                // this.buildingListForm.patchValue({
                //     "projectName": 'this.reqestObject.projectName',
                //  });
                this.userViewForm.patchValue({
                    "houseAdd": this.addressObject[0].house_address,
                    "buildingNumber": this.addressObject[0].building_number,
                    "strName": this.addressObject[0].street_name,
                    "locality": this.addressObject[0].locality,
                    "landmark": this.addressObject[0].landmark,
                    "selectState": this.addressObject[0].state,
                    "selectDivision": this.addressObject[0].division,
                    "selectDistrict": this.addressObject[0].district,
                    "selectTaluka": this.addressObject[0].taluka,
                    "selectVillage": this.addressObject[0].village,
                    "pinCode": this.addressObject[0].pincode
                },
                    err => {
                        console.log("not done");
                    }
                );
            },
            err => {
                console.log("not done");
            }
            );



        this.responseObject = this.configService.getView();
        console.log("request", this.responseObject);
        this.userViewForm.patchValue({
            "firstName": this.responseObject.first_name,
            "lastName": this.responseObject.last_name,
            "panNumber": this.responseObject.pan_number,
            "department": this.responseObject.department_name,
            "designation": this.responseObject.designation,
            "role": this.responseObject.role,
            "reportingTo": this.responseObject.reporting_to,
            "mobileNumber": this.responseObject.phone_number,
            "officeNumber": this.responseObject.office_number,
            "faxNumber": this.responseObject.fax_number,
            "emailId": this.responseObject.email_id,




        },
            err => {
                console.log("not done");
            }
        );
    }
}

