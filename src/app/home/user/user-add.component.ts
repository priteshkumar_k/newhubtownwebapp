/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Home Routing Module
	Date Created	: 	05 June 2017
	Date Modified	: 	05 June 2017
*/

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { UserServices } from "../user/user.services";
import { ConfigService } from "../../config-service";
@Component({
    templateUrl: './user-add.component.html',
    styleUrls: ['./user.component.css'],
    providers: [UserServices]
})

export class UserRegistration {
    private userRegistrationForm: FormGroup;
    public reqestObject: any = {};


    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,
        private userServices: UserServices,
        private configService: ConfigService
    ) {

        this.userRegistrationForm = formBuilder.group({
            "userName": ["", [Validators.required]],
            "dob": ["", [Validators.required]],
            "firstName": ["", [Validators.required]],
            "lastName": ["", [Validators.required]],
            "panNumber": ["", [Validators.required]],
            "password": ["", [Validators.required]],
            "houseAdd": ["", [Validators.required]],
            "buildingNumber": ["", [Validators.required]],
            "strName": ["", [Validators.required]],
            "locality": ["", [Validators.required]],
            "landmark": ["", [Validators.required]],
            "selectState": ["", [Validators.required]],
            "selectDivision": ["", [Validators.required]],
            "selectDistrict": ["", [Validators.required]],
            "selectTaluka": ["", [Validators.required]],
            "selectVillage": ["", [Validators.required]],
            "pinCode": ["", [Validators.required]],
            "mobileNumber": ["", [Validators.required]],
            "officeNumber": ["", [Validators.required]],
            "faxNumber": ["", [Validators.required]],
            "emailId": ["", [Validators.required]],
            "department": ["", [Validators.required]],
            "designation": ["", [Validators.required]],
            "projects": ["", [Validators.required]],
            "reportingTo": ["", [Validators.required]],
            "role": ["", [Validators.required]],



        });

    }
    addUser() {
        console.log("In add button function");

        this.reqestObject = this.userRegistrationForm.value;
        console.log("request", this.reqestObject);
        this.userServices.addUserDetail(this.reqestObject)
            .subscribe((responseObject) => {
                console.log(responseObject);
                console.log("done");
                alert("User Added Successfully")
                this.router.navigate(['userList']);
                // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", this.errorMessageService.getErrorMessages().done);

            },
            err => {
                console.log("not done");
                alert("Unable to add User");


                // this.feedbackMessageComponent.updateMessage(true, null, "alert-danger", this.errorMessageService.getErrorMessages().notDone);

            }
            );
    }

}
