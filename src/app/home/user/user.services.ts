/*
Author :  Created on 25/02/2017
*/

import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { ConfigService } from "../../config-service";
import { Observable } from "rxjs";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";


@Injectable()
export class UserServices {

    private headers;
    private options;

    private apiUrls;
    // public getSetView:any = {};



    constructor(private configService: ConfigService, private http: Http, private router: Router) {

    }



    /************************     SELECTED PROGRAM SETTER AND GETTER ********************************/
    // setProgram(program: any) {
    //     if (program) {
    //         console.log("Program", program);
    //         this.clientSetupService.setClientUrl(program.clientSetupUrl);
    //         console.log("Url", program.clientSetupUrl);
    //         //this.setprogramUrl(program.programSetupUrl);
    //     }
    //     this.programBeingProcessed = program;
    //     console.log("Selected Program in service", this.programBeingProcessed)
    // }

    // getProgram() {
    //     return this.programBeingProcessed;
    // }
    /************************************************************************************************/



    // setView(viewobj){
    //     this.getSetView = viewobj;
    //     console.log(this.getSetView);
    // }
    // getView(){
    //     console.log(this.getSetView);
    //     return this.getSetView;
    // }




    /*******************************************ADD API FOR BUILDING REGISTRATION***************************/

    addUserDetail(requestObject): any {
        //let body = JSON.stringify({ 'foo': 'bar' });
        let headers = new Headers({
            'Content-Type': 'application/json'
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().addUser;

        console.log("in service", requestObject);
        console.log("in servnnnnice", reqObj);
        console.log("in servnnnnice", headers);
        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {

                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }
    /*******************************************ADD API FOR BUILDING REGISTRATION***************************/

    updateUserDetail(requestObject): any {
        console.log("in service");
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().individualRegistrationUpdate;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }

    viewUserDetail(requestObject): any {
        console.log("in view service");
        console.log(requestObject);
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;
        console.log("reqObj+++", reqObj);
        let url = this.configService.getAPIUrls().individualRegistrationView;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)
            //    console.log("before map");

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });


    }
    getUserAddressDetail(requestObject): any {
        console.log("in view service");
        console.log(requestObject);
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;
        console.log("reqObj+++", reqObj);
        let url = this.configService.getAPIUrls().getUserAddressDetail;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)
            //    console.log("before map");

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });


    }

    listUserDetail(requestObject): any {
        console.log("in service");
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().listUser;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    projectSelectionIndividualList(requestObject): any {
        console.log("in service");
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().projectSelectionIndividualList;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }
}