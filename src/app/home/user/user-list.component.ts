/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Home Routing Module
	Date Created	: 	13 June 2017
	Date Modified	: 	13 June 2017
*/
import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { UserServices } from "../user/user.services";
import { ConfigService } from "../../config-service";
@Component({
    templateUrl: './user-list.component.html',
    styleUrls: ['./user.component.css'],
    providers: [UserServices]
})

export class UserListComponent {

    private addUser: FormGroup;
    public projects: any = [];
    public userDetailArr: any = [];
    public abc: any = {};
    public reqestObject: any = {};
    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,
        private userServices: UserServices,
        private configService: ConfigService
    ) {
        this.addUser = formBuider.group({
            "selectProject": ["", [Validators.required]],
            "searchField": ["", [Validators.required]],
        })

    }
    addFormClick() {
        this.router.navigate(['adduser']);
    }
    ngOnInit() {
        this.projects = [];
        // this.BuildingRegistrationService.getsetValue;
        // this.buildingRegistrationService.getsetValue();
        console.log();

        console.log("In add button function");
        this.reqestObject = {};
        //this.reqestObject=this.buildingListForm.value;  
        console.log("request", this.reqestObject);
        this.userServices.listUserDetail(this.reqestObject)
            .subscribe((responseObject) => {
                console.log("done");
                console.log(responseObject);

                this.userDetailArr = responseObject;
                // this.buildingListForm.patchValue({
                //     "projectName": 'this.reqestObject.projectName',
                //  });
            },
            err => {
                console.log("not done");
            }
            );
    }
    projectSelectionIndividualList(newvalue) {
        this.userDetailArr = [];
        console.log(newvalue);
        console.log("In add button function");
        this.reqestObject = { newvalue };
        //this.reqestObject=this.buildingListForm.value;
        console.log("request", this.reqestObject);
        this.userServices.listUserDetail(this.reqestObject)
            .subscribe((responseObject) => {
                console.log("done");
                console.log(responseObject);

                // this.projects=responseObject;
                this.userDetailArr = responseObject;
                // this.buildingListForm.patchValue({
                //     "projectName": 'this.reqestObject.projectName',
                //  });
            },
            err => {
                console.log("not done");
            }
            );

    }


    viewUserDetail(userDetail) {
        this.abc = userDetail;//.buildingCode;
        console.log("build object here");
        console.log(this.abc = userDetail);

        //set here
        this.configService.setView(this.abc);
        this.router.navigate(['viewUserDetail']);

    }

    updateUserDetail(userDetail) {


        this.abc = userDetail;//.buildingCode;
        console.log("build object here");
        console.log(this.abc = userDetail);

        //set here
        this.configService.setView(this.abc);
        this.router.navigate(['updateUserDetail']);

    }
}


