/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Home Routing Module
	Date Created	: 	05 June 2017
	Date Modified	: 	05 June 2017
*/

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subject, ReplaySubject } from "rxjs";
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { ErrorMessageService } from "../../share/error-messages.service";



@Component({
    templateUrl: './showCompanyDetail.html',
    styleUrls: ['./showCompanyDetail.css'],

})
export class CompanyDetails implements OnInit {

    private organizationSetupAddForm: FormGroup;

    constructor(
        private formBuider: FormBuilder,
        private router: Router,
        private errorMessageService: ErrorMessageService,
        private formBuilder: FormBuilder,
    ) {
        this.organizationSetupAddForm = formBuilder.group({
            "orgName": ["", [Validators.required]],
            "panNumber": ["", [Validators.required]],
            "orgType": ["", [Validators.required]],
            "houseAdd": ["", [Validators.required]],
            "buildingNumber": ["", [Validators.required]],
            "strName": ["", [Validators.required]],
            "locality": ["", [Validators.required]],
            "landmark": ["", [Validators.required]],
            "selectState": ["", [Validators.required]],
            "selectDivision": ["", [Validators.required]],
            "selectDistrict": ["", [Validators.required]],
            "selectTaluka": ["", [Validators.required]],
            "selectVillage": ["", [Validators.required]],
            "pinCode": ["", [Validators.required]],
            "nameOfContactPer": ["", [Validators.required]],
            "officeNumber": ["", [Validators.required]],
            "faxNumber": ["", [Validators.required]],
            "emailId": ["", [Validators.required]],
            "webSite": ["", [Validators.required]],
        });
    }
    open() {

    }
    ngOnInit() {

    }
    /********************************   ONCLICK LISTENERS   ******************************/




}
