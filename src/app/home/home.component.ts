/*
	Author			:	PriteshKumar Kanaujiya
	Description		: 	Home component
	Date Created	: 	10 June 2017
	Date Modified	: 	10 June 2017
*/
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
// import { ConfigService } from "../config.service";
import { AppComponent } from "../app.component";

// import { Sample } from "./html-editor.component";


@Component({
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
    titleOfScreen = 'Home';
    modules: string[];
    loggedInUser: any;
    private userRole: string;
    public isNotificationlShown: boolean = false;
    public isShow: boolean = false;
    public isShow1: boolean = false;
    //To show response message from api calls
    // @ViewChild(Sample)
    // private sample: Sample;


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        // private configServ: ConfigService,
        private appComponent: AppComponent) {
        console.log("Home ");
    }

    ngOnInit() {

    }

    appConfig() {
        this.router.navigate(['appConfig'])
    }
    locationTracking() {
        this.router.navigate(['locationTracking'])
    }
    employeeRecord() {
        this.router.navigate(['employeeRecord'])
    }
    goBack() {
        this.router.navigate(['login']);
    }

    navigate(pageName) {
        this.router.navigate([pageName]);
    }
    onClickNotification() {
        this.isNotificationlShown = true;
    }

    addUser() {
        this.router.navigate(['userList']);
    }
    addProject() {
        this.router.navigateByUrl('list-page');
        // this.router.navigate(['.add-project']);
    }
    addDepartment() {
        this.router.navigate(['departmentList']);
    }

    onDashboard() {
        this.router.navigate(['dashboard']);
    }
    isShowUser() {
        if (this.isShow == false) {
            this.isShow = true;
        }
        else {
            this.isShow = false;
        }
    }

    isShowUserOrg() {
        if (this.isShow1 == false) {
            this.isShow1 = true;
        }
        else {
            this.isShow1 = false;
        }
    }
}