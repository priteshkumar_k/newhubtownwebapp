/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Add Project component
	Date Created	: 	08 June 2017
	Date Modified	: 	08 June 2017
*/
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { ErrorMessageService } from "../../share/error-messages.service";

interface ResponseObject {
    statusCode: number;
    message: string;
    result: any;
}


@Component({
    templateUrl: './project-add.component.html',
    styleUrls: ['./project-add.component.css'],
    providers: []//LoginService
})
export class ProjectAddComponent {



    private projectSetupAddForm: FormGroup;


    public showLoader: boolean = false;
    public loggedInUserInfo: any = {};



    constructor(private route: ActivatedRoute,
        private router: Router,
        private errorMessageService: ErrorMessageService,
        private formBuilder: FormBuilder,
    ) {
        this.projectSetupAddForm = formBuilder.group({
            "projectStatus": ["", [Validators.required]],
            "projectName": ["", [Validators.required]],
            "projectType": ["", [Validators.required]],
            "dateOfCompletion": ["", [Validators.required]],
            "revDateOfCompletion": ["", [Validators.required]],
            "litigationRelated": ["", [Validators.required]],
            "plotNumber": ["", [Validators.required]],
            "areainsq": ["", [Validators.required]],
            "aggareainsq": ["", [Validators.required]],
            "proposeNotSanction": ["", [Validators.required]],
            "boundaryEast": ["", [Validators.required]],
            "boundaryWest": ["", [Validators.required]],
            "boundarySouth": ["", [Validators.required]],
            "boundaryNorth": ["", [Validators.required]],
            "totalFsi": ["", [Validators.required]],
            "selectState": ["", [Validators.required]],
            "selectDivision": ["", [Validators.required]],
            "selectDistrict": ["", [Validators.required]],
            "selectTaluka": ["", [Validators.required]],
            "selectVillage": ["", [Validators.required]],
            "pinCode": ["", [Validators.required, Validators.pattern('[0-9]{6}')]],
            "bankName": ["", [Validators.required]],
            "branchName": ["", [Validators.required]],
            "IfscCode": ["", [Validators.required]],
            "accNumber": ["", [Validators.required]],
            "bankAddress": ["", [Validators.required]],
            "projectCode": ["", [Validators.required]],
        });
    }

    onDashboard() {

        this.router.navigate(['../home/dashboard']);

    }

}