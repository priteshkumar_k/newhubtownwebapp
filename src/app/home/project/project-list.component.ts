/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Add Project component
	Date Created	: 	10 June 2017
	Date Modified	: 	10 June 2017
*/
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { ErrorMessageService } from "../../share/error-messages.service";

interface ResponseObject {
    statusCode: number;
    message: string;
    result: any;
}


@Component({
    templateUrl: './project-list.component.html',
    styleUrls: ['./project-list.component.css'],
    providers: []//LoginService
})
export class ProjectListComponent {


    public projectLisrForm: FormGroup;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,

        private errorMessageService: ErrorMessageService,
    ) {
        this.projectLisrForm = formBuilder.group({
            "selectProject": ["", [Validators.required]],
            "searchField": ["", [Validators.required]],
        })
    }
    addFormClick() {
        this.router.navigate(['add-project']);
    }

}