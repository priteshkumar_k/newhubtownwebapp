/*
    Author : Sumit Ghotekar.
    Description : User registration component.
    Date of creation : 17/05/2017
 */

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";
import { AppConfigServices } from "../appConfig/appConfig.services";
import { ConfigService } from "../../config-service";
@Component({
    templateUrl: './appConfig.html',
    styleUrls: ['./appConfig.css'],
    providers: [AppConfigServices]
})

export class AppConfig {
    private appConfig: FormGroup;
    public requestObject: any = {};


    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,
        private appConfigServices: AppConfigServices


    ) {

        this.appConfig = formBuilder.group({
            "timeInterval": ["", [Validators.required]],
            "startTime": ["", [Validators.required]],
            "endTime": ["", [Validators.required]],
            "sentTime": ["", [Validators.required]],
            "designations": ["", [Validators.required]],
            "gpsRadius": ["", [Validators.required]],
            "loc3": ["", [Validators.required]],
            "loc2": ["", [Validators.required]],
            "loc1": ["", [Validators.required]],
            "selectDistrict": ["", [Validators.required]],
            "selectTaluka": ["", [Validators.required]],
            "selectVillage": ["", [Validators.required]],
            "pinCode": ["", [Validators.required]],
            "mobileNumber": ["", [Validators.required]],
            "officeNumber": ["", [Validators.required]],
            "faxNumber": ["", [Validators.required]],
            "emailId": ["", [Validators.required]],
            "webSite": ["", [Validators.required]],
        });

    }
    updateAppConfigDetail(){
        console.log("in update");

        this.requestObject = this.appConfig.value;
        console.log("request", this.requestObject);
        this.appConfigServices.updateAppConfigDetail(this.requestObject)
            .subscribe((responseObject) => {
                console.log(responseObject);
                console.log("done");
                alert("Updated Successfully")
                this.router.navigate(['userList']);
                // this.feedbackMessageComponent.updateMessage(true, responseObject.message, "alert-danger", this.errorMessageService.getErrorMessages().done);

            },
            err => {
                console.log("not done");
                alert("Unable to add User");


                // this.feedbackMessageComponent.updateMessage(true, null, "alert-danger", this.errorMessageService.getErrorMessages().notDone);

            }
            );
    }

}