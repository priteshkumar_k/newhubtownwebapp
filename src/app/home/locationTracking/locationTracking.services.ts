/*
Author :  Created on 25/02/2017
*/

import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { ConfigService } from "../../config-service";
import { Observable } from "rxjs";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";


@Injectable()
export class LocationTrackingServices {

    private headers;
    private options;
    private apiUrls;
    // public getSetView:any = {};



    constructor(private configService: ConfigService, private http: Http, private router: Router) {

    }


    locationTrackingDetail(requestObject): any {
        //let body = JSON.stringify({ 'foo': 'bar' });
        let headers = new Headers({
            'Content-Type': 'application/json'
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().locationTrackingDetail;
        console.log("url is", url)
        console.log("in service", requestObject);
        console.log("in servnnnnice", reqObj);
        console.log("in servnnnnice", headers);
        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {

                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }
    /*******************************************ADD API FOR BUILDING REGISTRATION***************************/

    latlongDetail(requestObject): any {
        console.log("in service");
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().latlongDetail;
        console.log("req obj", reqObj);
        console.log(url);
        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });

    }

    projectCostRegistrationView(requestObject): any {
        console.log("in view service");
        console.log(requestObject);
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;
        console.log("reqObj+++", reqObj);
        let url = this.configService.getAPIUrls().projectCostRegistrationView;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)
            //    console.log("before map");

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });


    }

    projectCostList1(requestObject): any {
        console.log("in service");
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().projectCostList1;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }

    projectSelectionProjectCost(requestObject): any {
        console.log("in service");
        let headers = new Headers({
            'Content-Type': 'application/json',
            //     'Authorization': 'Bearer ' + this.configService.loggedInUserInfo.token
        });
        let options = new RequestOptions({ headers: headers });
        let reqObj = requestObject;

        let url = this.configService.getAPIUrls().projectSelectionProjectCost;

        return this.http.post(url, reqObj, options)
            // return this.http.post(url, reqObj)

            .map((res: Response) => {
                console.log("inside map", res.json());
                return res.json();
            })
            .catch((error: any) => {
                console.log("inside catch");
                return Observable.throw(error.json().error || JSON.parse(error._body).message || error.statusText || "Some Error Occurred");
            });
    }
}