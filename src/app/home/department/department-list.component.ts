/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Home Routing Module
	Date Created	: 	05 June 2017
	Date Modified	: 	05 June 2017
*/

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";

@Component({
    templateUrl: './department-list.component.html',
    styleUrls: ['./department.component.css'],
    providers: []
})

export class DepartmentListComponent {

    private departmentListForm: FormGroup;
    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,
    ) {
        this.departmentListForm = formBuider.group({
            "selectProject": ["", [Validators.required]],
            "searchField": ["", [Validators.required]],
        })

    }

    addFormClick() {
        this.router.navigate(['departmentRegistration']);
    }
}
