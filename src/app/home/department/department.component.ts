/*
	Author			:	Priteshkumar Kanaujiya
	Description		: 	Home Routing Module
	Date Created	: 	05 June 2017
	Date Modified	: 	05 June 2017
*/
import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router"
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../../share/error-messages.service";
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from "@angular/forms";

@Component({
    templateUrl: './department.component.html',
    styleUrls: ['./department.component.css'],
    providers: []
})

export class DepartmentRegistration {
    private departmentRegistrationForm: FormGroup;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private formBuider: FormBuilder,
        private errorMessageService: ErrorMessageService,

    ) {

        this.departmentRegistrationForm = formBuilder.group({
            "deptCode": ["", [Validators.required]],
            "deptName": ["", [Validators.required]],
            "deptDesp": ["", [Validators.required]],
            "deptHead": ["", [Validators.required]],
            "strDate": ["", [Validators.required]],
            "endDate": ["", [Validators.required]],
        });

    }
}