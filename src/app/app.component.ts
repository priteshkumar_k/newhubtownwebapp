
import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Rx"
import { Router, NavigationEnd } from "@angular/router";

// import { ApplicationAccessAuthentication } from "./application-access-authentication.service";
// import { Device } from "ng2-device-detector/dist/services/ng2-device.service";
// import {AppService} from "./app.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']

})
export class AppComponent {
    title = 'app works!';

    moduleName: string;
    loggedInUser: any;
    pageClass: string;
    private userRole: string = "";
    private accessErrorMsg: string = "";
    deviceInfo: any;
    userLocationInfo: any;
    frontEndUserInfo: any;

    public editorContent: any = "";
    isShowAccessMessage: boolean = false;

    constructor(
        // private configServ: ConfigService,
        private router: Router,
        // private accessComponent: ApplicationAccessAuthentication,
        // private device:Device,
        // private appService: AppService
    ) {
        // console.log("Device", this.device);
        // this.deviceInfo = this.device;
        // this.getIP();



    }

    ngOnInit() {

        // To apply classes based on route
        // this.router.events.filter(event => event instanceof NavigationEnd)
        //     .subscribe((event:NavigationEnd) => {
        //         console.log("AppComponent Route: ", event.url);
        //         console.log("Calling module name from app main compenent");
        //         // this.moduleName = this.configServ.getModuleName();
        //         console.log("Module name",this.moduleName);
        //         if(event.url === '/login' || event.url ==='/'){
        //             this.pageClass = "loginPage";
        //         } else if(event.url === '/home'){
        //             this.pageClass = "homePage";
        //         } else {
        //             this.pageClass = "innerPage";
        //         }
        //     }
        // );
        // if(event.url === '/login' || event.url ==='/'){
        this.pageClass = "loginPage";


        // To show access restriction message 
        // this.accessComponent.subject
        //     .subscribe(value => {
        //         this.isShowAccessMessage =true;
        //         this.accessErrorMsg = value;
        //         setTimeout(() =>{
        //                 this.isShowAccessMessage = false;
        //             },3500
        //         );     
        //     });

        // Get logged in user info
        // this.loggedInUser = this.configServ.loggedInUserInfo;

        console.log("@@@@  Logged In User @@@@", this.loggedInUser);

        // To show logged-in user information in header
        if (this.loggedInUser) {
            if (this.loggedInUser.annectosRole.roleName) {
                this.userRole = this.loggedInUser.annectosRole.roleName;
            }
        }
        // this.configServ.userInfoChangeEvent.subscribe(data => {
        //         this.loggedInUser = data;
        // });
    }

}
