/*
	Author			:	Nitesh Kamble
	Description		: 	Setting Login module for all the files required for login page
	Date Created	: 	01 May 2017
	Date Modified	: 	01 May 2017
*/

import { NgModule } from "@angular/core";
import { DashBoardComponent } from "./dashboard.component";
// import { SignUpComponent } from "./signup.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DashBoardCommonComponent } from "./dashboard-commonpage.component";
import { MaterialModule } from '@angular/material';
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ChartsModule } from 'ng2-charts/ng2-charts';


@NgModule({

	declarations: [DashBoardComponent,
		DashBoardCommonComponent,

	],//, SignUpComponent, ProgramSelection, ClientProgramSelection,ForgotPasswordComponent
	imports: [CommonModule, ChartsModule, ReactiveFormsModule, HttpModule, DashboardRoutingModule, MaterialModule.forRoot(), ModalModule.forRoot()],//MaterialModule,FooterModule
	exports: [DashBoardComponent],

	providers: [],//LoginService,FeedbackMessageComponent,AppFooterComponent
	entryComponents: []//ProgramSelection, ClientProgramSelection, ForgotPasswordComponent
})
export class DashboardModule {
}