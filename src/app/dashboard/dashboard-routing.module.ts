/*
	Author			:	Sumit Ghotekar
	Description		: 	Dashboard Routing Module
	Date Created	: 	05 May 2017
	Date Modified	: 	05 May 2017
*/
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DashBoardComponent } from "./dashboard.component";
import { DashBoardCommonComponent } from "./dashboard-commonpage.component";
import { ProjectAddComponent } from "../home/project/project-add.component";
import { ProjectListComponent } from "../home/project/project-list.component";

export const dashBoardRoutes: Routes = [
    {
        path: '',
        component: DashBoardComponent,
        children: [
            {
                path: 'addProject',
                component: ProjectAddComponent,
            }
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(dashBoardRoutes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {

}