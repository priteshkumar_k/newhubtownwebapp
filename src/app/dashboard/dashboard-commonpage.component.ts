/*
	Author			:	Nitesh Kamble
	Description		: 	Dashboard Common component
	Date Created	: 	05 May 2017
	Date Modified	: 	05 May 2017
*/
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

interface ResponseObject {
    statusCode: number;
    message: string;
    result: any;
}


@Component({
    templateUrl: './dashboard-commonpage.component.html',
    styleUrls: ['./dashboard.component.css'],
    providers: []//LoginService
})
export class DashBoardCommonComponent {

    private username: string;
    private password: string;
    private loginMsgErr: boolean = false;
    private loginMsgErrBlocked: boolean = false;
    private IsSubmitBtnErr: boolean = false;

    private loginForm: FormGroup;

    public showLoader: boolean = false;
    public loggedInUserInfo: any = {};



    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
    ) {
        console.log("in dashboard commn")
    }

}