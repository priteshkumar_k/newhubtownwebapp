/*
	Author			:	Sumit Ghotekar
	Description		: 	Dashboard component
	Date Created	: 	03 May 2017
	Date Modified	: 	03 May 2017
*/
import { Component, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ChartsModule } from 'ng2-charts/ng2-charts';





interface ResponseObject {
    statusCode: number;
    message: string;
    result: any;
}


@Component({
    selector: 'demo-modal-auto-shown',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
    providers: []//LoginService
})
export class DashBoardComponent {

    @ViewChild('autoShownModal') public autoShownModal: ModalDirective;
    public isModalShown: boolean = false;

 private dashboard: FormGroup;
    private username: string;
    private password: string;
    private loginMsgErr: boolean = false;
    private loginMsgErrBlocked: boolean = false;
    private IsSubmitBtnErr: boolean = false;

    private loginForm: FormGroup;

    public showLoader: boolean = false;
    public loggedInUserInfo: any = {};

    // for graph
     public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Average % Attendance'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Average Check-in Time'}
  ];



    constructor(private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
    ) {
        

        this.dashboard = formBuilder.group({
            "date": ["", [Validators.required]],
            "employeeId": ["", [Validators.required]],
           
        });

    }
     public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
 
  public randomize():void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }


    addProject() {
        this.router.navigateByUrl('home/list-page');
        // this.router.navigate(['.home/add-project']);
    }
    public showModal(): void {
        this.isModalShown = true;
    }

    public hideModal(): void {
        this.autoShownModal.hide();
    }

    public onHidden(): void {
        this.isModalShown = false;
    }
    
}