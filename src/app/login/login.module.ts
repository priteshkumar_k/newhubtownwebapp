/*
	Author			:	PriteshKumar Kanaujiya
	Description		: 	Setting Login module for all the files required for login and signup page
	Date Created	: 	01 June 2017
	Date Modified	: 	01 June 2017
*/

import { NgModule } from "@angular/core";
import { LoginComponent } from "./login.component";
import { SignUpComponent } from "./signup.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
// import {MaterialModule} from '@angular/material';

// import { FeedbackMessageComponent } from "../shared-components/feedback-message.component";

@NgModule({

	declarations: [LoginComponent,
		SignUpComponent

	],
	imports: [CommonModule, ReactiveFormsModule, HttpModule, MaterialModule],
	exports: [LoginComponent, SignUpComponent],

	providers: [],
	entryComponents: []
})
export class LoginModule {
}