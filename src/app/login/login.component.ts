/*
	Author			:	Nitesh Kamble
	Description		: 	Login component
	Date Created	: 	01 May 2017
	Date Modified	: 	01 May 2017
*/

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { MaterialModule } from '@angular/material';
import { ErrorMessageService } from "../share/error-messages.service";
import { Observable, Subject, ReplaySubject } from "rxjs";
import { LoginServices } from "../login/login.services";
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


// import {ConfigService} from "../config.service";
// import { ErrorMessageService } from "../shared/error-messages.service";
// import * as CryptoJS from "../../../node_modules/crypto-js";
// import {MdDialog, MdDialogRef, MdDialogConfig} from '@angular/material';
// import {ForgotPasswordComponent} from "./forgot-password.component";

interface ResponseObject {
    statusCode: number;
    message: string;
    result: any;
}


@Component({
    //selector: 'login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [LoginServices]//LoginService
})
export class LoginComponent {
    reqestObject: any;

    private username: string;
    private password: string;
    private loginMsgErr: boolean = false;
    private loginMsgErrBlocked: boolean = false;
    private IsSubmitBtnErr: boolean = false;
    public isShow: boolean = false;
    private loginForm: FormGroup;
    public isForgotPassword: boolean = true;
    public isLogin: boolean = false;

    public showLoader: boolean = false;
    public loggedInUserInfo: any = {};



    constructor(private route: ActivatedRoute,
        private router: Router,
        private loginServices: LoginServices,
        private formBuilder: FormBuilder,
        // private configService: ConfigService,
        // private errorMessageService: ErrorMessageService,
        // public dialog: MdDialog
    ) {

        this.loginForm = formBuilder.group({
            "email": ["", Validators.required],
            "password": ["", Validators.required],
            "verifyEmail": ["", Validators.required]
        });

        //  if (this.configService.loggedInUserInfo!== null) {
        //      this.router.navigate(['../home'], { relativeTo: this.route });
        //     console.log("loggedInUserInfo object received", this.loggedInUserInfo);
        //  } else {
        //      this.router.navigate(['../login'], { relativeTo: this.route }); 
        //     console.log("Module & Screen INFO not found in loggedInUserInfo");

        // }
    }
    checkInputs(): void {
        if ((this.loginForm.value.username != null && this.loginForm.value.username != "") &&
            (this.loginForm.value.password != null && this.loginForm.value.password != "")) {
            // this.doLogin();
            console.log("All Okay");
        } else {
            console.log("All not Okay");
            this.loginMsgErr = false;
            this.loginMsgErrBlocked = false;
        }
    }
    toSignup() {
        this.router.navigate(['./signup']);
    }
    onHome() {
        this.router.navigate(['../home']);
    }
    toForgotPassword() {
        console.log("in forgot password");
        this.isForgotPassword = false;
        this.isLogin = true;

    }
    toLogin() {
        this.isForgotPassword = true;
        this.isLogin = false;
    }
    login() {
        console.log("in register ts")
        this.reqestObject = this.loginForm.value;
        console.log("request", this.reqestObject);
        if (this.checkInputs) {
            this.loginServices.userLogin(this.reqestObject)
                .subscribe((responseObject) => {
                    console.log(responseObject);
                    console.log("done");
                    if (responseObject.statusCode == 1) {
                        alert("invalid Username  Password")

                    }
                    else {
                        alert("login Successful")
                        this.onHome();
                    }


                },
                err => {
                    console.log("login Failed");
                    alert("Login failed");
                    // this.feedbackMessageComponent.updateMessage(true, null, "alert-danger", this.errorMessageService.getErrorMessages().notDone);

                }
                );

        }
        else {
            alert("All fields are mandatory")
        }
    }
    //call to verify e-mail and send password to registered mail-id
    forgotPassword() {

        console.log("in forgot password")
        this.reqestObject = this.loginForm.value;
        console.log("request", this.reqestObject);
        if ((this.loginForm.value.verifyEmail != null && this.loginForm.value.verifyEmail != "")) {
            this.loginServices.forgotPassword(this.reqestObject)
                .subscribe((responseObject) => {
                    console.log(responseObject);
                    console.log("done");
                    if (responseObject.statusCode == 1) {
                        alert("Entered Email Id is not Registered")

                    }
                    else {
                        alert("Password is sent to your Registered Email Address.")
                        this.toLogin();
                    }


                },
                err => {
                    console.log("Failed to connect to server");
                    alert("Failed to connect to server");
                    // this.feedbackMessageComponent.updateMessage(true, null, "alert-danger", this.errorMessageService.getErrorMessages().notDone);

                }
                );

        }
        else {
            alert("Please Enter the Registered Email Id")
        }

    }
}
