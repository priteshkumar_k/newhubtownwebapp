/*
    AUTHOR      : Priteshkumar Kanaujiya
    DESCRIPTION :   Component for signup page
    Date Created	: 	05 June 2017
	Date Modified	: 	05 June 2017
*/

import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subject, ReplaySubject } from "rxjs";
import { BrowserModule } from '@angular/platform-browser';
import { ErrorMessageService } from "../share/error-messages.service";
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { LoginServices } from "../login/login.services";

@Component({
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css'],
    providers: [LoginServices]
})
export class SignUpComponent implements OnInit {

    private signupForm: FormGroup;
    reqestObject: any;
    constructor(
        private formBuider: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private elementRef: ElementRef,
        private formBuilder: FormBuilder,
        private errorMessageService: ErrorMessageService,
        private loginServices: LoginServices
    ) {
        this.signupForm = formBuilder.group({
            "companyName": ["", [Validators.required]],
            "companyRegNumber": ["", [Validators.required]],
            "userName": ["", [Validators.required]],
            "address": ["", [Validators.required]],
            "password": ["", [Validators.required]],
            "conPassword": ["", [Validators.required]],
            "phoneNumber": ["", [Validators.required]],
            "emailId": ["", [Validators.required]],
        });
    }
    open() {

    }
    ngOnInit() {

    }

    goToLogin() {
        this.router.navigateByUrl('/login');
    }

    registerCompany() {
        console.log("in register ts")
        this.reqestObject = this.signupForm.value;
        console.log("request", this.reqestObject);
        if (this.reqestObject.companyName != "" || this.reqestObject.userName != "" || this.reqestObject.Password != "" || this.reqestObject.conPassword != "") {
            this.loginServices.registerCompany(this.reqestObject)
                .subscribe((responseObject) => {
                    console.log(responseObject);
                    console.log("done");
                    if (responseObject.statusCode == 1) {
                        alert("Data Not Found")

                    }
                    else {
                        alert("registration Successful")
                        this.goToLogin();
                    }


                },
                err => {
                    console.log("Registration Failed");
                    alert("Registration failed");
                    // this.feedbackMessageComponent.updateMessage(true, null, "alert-danger", this.errorMessageService.getErrorMessages().notDone);

                }
                );

        }
        else {
            alert("All fields are mandatory")
        }
    }

}


