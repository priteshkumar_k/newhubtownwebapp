/**
 * Created by Sumit GHotekar
 * Created Date :  2017-06-08.
 */
import { Injectable, OnInit, EventEmitter } from "@angular/core";
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { Observable, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

/*@Injectable :
    It lets Angular know that a class can be used with the dependency injector.
*/
@Injectable()
export class ConfigService {
    private apiBasePath: string;
    private servicePorts;
    private apiUrls;
    private objID: string = "";
    public getSetView: any = {};

    constructor(private http: Http) {
        this.objID = "id-" + Date.now();
        this.setAPIBasePath();
        this.setServicePorts();
        this.setAPIUrls();
    }

    setView(viewobj) {
        this.getSetView = viewobj;
        console.log(this.getSetView);
    }
    getView() {
        console.log(this.getSetView);
        return this.getSetView;
    }

    private setAPIBasePath() {

        // this.apiBasePath = "http://localhost";
        this.apiBasePath = "http://localhost";
    }

    public getAPIBasePath() {
        return this.apiBasePath;
    }

    public setServicePorts() {
        this.servicePorts = {};

        //this.servicePorts.configuration = 8081;
        this.servicePorts.configuration = 8000;


    }

    public getServicePorts() {

        return this.servicePorts;
    }

    setAPIUrls() {
        this.apiUrls = {};

        this.apiUrls.latlongDetail = this.apiBasePath + ":" + this.servicePorts.configuration + "/LatLongDetail/get";
        this.apiUrls.locationTrackingDetail = this.apiBasePath + ":" + this.servicePorts.configuration + "/trackList/get";
        this.apiUrls.employeeRecordDetail = this.apiBasePath + ":" + this.servicePorts.configuration + "/employeeRecord/get";
        this.apiUrls.registerCompany = this.apiBasePath + ":" + this.servicePorts.configuration + "/companyRegistration/add";
        this.apiUrls.userLogin = this.apiBasePath + ":" + this.servicePorts.configuration + "/login";
        this.apiUrls.forgotPassword = this.apiBasePath + ":" + this.servicePorts.configuration + "/forgotPassword";
        this.apiUrls.addUser = this.apiBasePath + ":" + this.servicePorts.configuration + "/addUser";
        this.apiUrls.listUser = this.apiBasePath + ":" + this.servicePorts.configuration + "/listUser";
        this.apiUrls.addUserAddressDetail = this.apiBasePath + ":" + this.servicePorts.configuration + "/addUserAddress";
        this.apiUrls.getUserAddressDetail = this.apiBasePath + ":" + this.servicePorts.configuration + "/getUserAddress";
        this.apiUrls.updateAppConfigDetail=this.apiBasePath + ":" +this.servicePorts.configuration + "/updateAppConfigDetail";

    }

    getAPIUrls() {
        return this.apiUrls;
    }

    /***********************************     GET STATES     *******************************************/
    // getStates(): any {
    //     let url = this.getAPIUrls().getStates;
    //     let reqObj = {
    //         searchString: ""
    //     };

    //     let states: string[] = [];

    //     this.callApi(url, reqObj).subscribe(
    //         (responseObject) => {
    //             let responseCodes = this.getConstants();
    //             // console.log("responseObject", responseObject);
    //             if (responseObject.statusCode === responseCodes.RESP_SUCCESS) {
    //                 for (let i = 0; i < responseObject.result.length; i++) {
    //                     states[i] = responseObject.result[i].stateName;
    //                 }
    //                 this.states.next(states);
    //             }
    //             else {
    //                 states[0] = "Error fetching states";
    //                 this.states.next(states);
    //             }
    //         },
    //         err => {
    //             console.log("err ", err);
    //         }
    //     );
    // }

}
