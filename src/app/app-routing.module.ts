/*
	Author			:	priteshkumar
	Description		: 	Routes for all the component will be setup here
	Date Created	: 	01 June 2017
	Date Modified	: 	01 June 2017
*/


import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { DashBoardComponent } from "./dashboard/dashboard.component";
import { SignUpComponent } from "./login/signup.component";

import { HomeComponent } from "./home/home.component";



export const appRoutes: Routes = [

	{ path: '', pathMatch: 'full', redirectTo: 'login' },
	{ path: 'login', pathMatch: 'full', component: LoginComponent },

	{ path: 'signup', pathMatch: 'full', component: SignUpComponent },


	{
		path: 'home',
		loadChildren: 'app/home/home.module#HomeModule'
	},


];

@NgModule({
	imports: [RouterModule.forRoot(appRoutes)],
	exports: [RouterModule]
})
export class AppRoutingModule {

}