import { Rera.WebApp.UiPage } from './app.po';

describe('rera.web-app.ui App', () => {
  let page: Rera.WebApp.UiPage;

  beforeEach(() => {
    page = new Rera.WebApp.UiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
